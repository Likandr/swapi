# Swapi

Versions of used libraries:

| lib | version |
| ------ | ------ |
| support | 1.0.0 | 
| appcompat | 1.0.2 | 
| butterknife | 9.0.0-rc1 |
| arch | 2.0.0 | 
| multidex | 2.0.0 | 
| room | 2.1.0-alpha02 | 
| core_ktx | 1.0.1 | 
| constraint_layout | 2.0.0-alpha2 | 
| dagger | 2.15 | 
| leak_canary | 1.3.1 | 
| okhttp | 3.9.0 | 
| picasso | 2.71828 | 
| retrofit | 2.3.0 | 
| rx_android | 2.0.2 | 
| rx_java | 2.1.0 | 
| rx_lifecycle | 0.3.0 | 
| gson | 2.8.2 | 