package com.likandr.swapi.common.base

import android.os.Bundle
import android.util.Log
import androidx.annotation.IdRes
import androidx.annotation.Nullable
import androidx.core.view.ViewCompat
import androidx.fragment.app.FragmentTransaction
import androidx.transition.Fade
import butterknife.ButterKnife
import butterknife.Unbinder
import com.likandr.swapi.common.FragmentParams
import com.likandr.swapi.common.Layout
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.Disposable

abstract class BaseActivity : DaggerAppCompatActivity() {

    private var mUnbinder: Unbinder? = null

    var currentFragmentTag: String? = null
    private var d: Disposable? = null

    @IdRes
    abstract fun contentRes(): Int

    var mFade = Fade()
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        Log.d("BaseActivity", "onCreate")
        super.onCreate(savedInstanceState)

        val cls = javaClass
        if (!cls.isAnnotationPresent(Layout::class.java)) return
        val annotation = cls.getAnnotation(Layout::class.java)
        val layout = annotation as Layout
        setContentView(layout.id)

        mUnbinder = ButterKnife.bind(this)

        beginWith()
    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder!!.unbind()
        if (d != null) d!!.dispose()
    }

    abstract fun beginWith()

    fun initFragment(fragmentParams: FragmentParams) {
        currentFragmentTag = fragmentParams.fragmentTag

        if (isDestroyed) {
            return
        }

        val fragment = getFragment(fragmentParams)
        val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()

        if (fragmentParams.animationItems != null) {
            fragmentTransaction.apply {
                for (param in fragmentParams.animationItems!!) {
                    addSharedElement(
                        param.sharedElement,
                        ViewCompat.getTransitionName(param.sharedElement)!!
                    )
                }
            }
        }

        fragmentTransaction.addToBackStack(currentFragmentTag).replace(contentRes(), fragment!!).commit()

        d = fragment.changeFragment().subscribe(this::initFragment)
    }

    open fun getFragment(fragmentParams: FragmentParams): BaseFragment? {
        return null
    }
}