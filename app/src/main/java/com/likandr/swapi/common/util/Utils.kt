package com.likandr.swapi.common.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object Utils {

    fun isNetworkAvailable(context: Context): Boolean {
        val networkTypes = intArrayOf(0, 1)
        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            for (networkType in networkTypes) {
                val netInfo = cm.getNetworkInfo(networkType)
                if (netInfo != null && netInfo.state == NetworkInfo.State.CONNECTED) {
                    return true
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

        return false
    }
}