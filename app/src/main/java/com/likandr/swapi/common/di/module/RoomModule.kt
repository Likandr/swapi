package com.likandr.swapi.common.di.module

import android.content.Context
import androidx.room.Room
import com.likandr.swapi.data.db.PeopleDao
import com.likandr.swapi.data.db.SwapiDatabase
import dagger.Module
import dagger.Provides

@Module
class RoomModule {

    @Provides
    fun providesRoomDatabase(context: Context): SwapiDatabase =
        Room.databaseBuilder(context, SwapiDatabase::class.java, "swapi-db").build()

    @Provides
    fun providesSwapiDao(swapiDatabase: SwapiDatabase): PeopleDao {
        return swapiDatabase.getPeopleDao()
    }
}
