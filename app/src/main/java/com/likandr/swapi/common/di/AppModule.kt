package com.likandr.swapi.common.di

import com.likandr.swapi.common.di.module.NetworkModule
import com.likandr.swapi.common.di.module.RoomModule
import com.likandr.swapi.common.di.module.ViewModelModule
import dagger.Module

@Module(includes = [
    RoomModule::class,
    NetworkModule::class,
    ViewModelModule::class
])
class AppModule
