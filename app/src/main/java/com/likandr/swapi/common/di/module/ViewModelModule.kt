package com.likandr.swapi.common.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.likandr.swapi.common.util.ViewModelFactory
import com.likandr.swapi.common.util.ViewModelKey
import com.likandr.swapi.ui.detail.DetailViewModel
import com.likandr.swapi.ui.main.SearchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds @IntoMap @ViewModelKey(SearchViewModel::class)
    internal abstract fun bindSearchViewModel(listViewModel: SearchViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(DetailViewModel::class)
    internal abstract fun bindDetailViewModel(detailViewModel: DetailViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}