package com.likandr.swapi.common

import android.view.View

data class FragmentParams(var fragmentTag : String, var args: Any?, var animationItems : List<AnimationParam>?)

class AnimationParam(var sharedElement : View, var name :String)
