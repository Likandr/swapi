package com.likandr.swapi.common

import android.content.Context
import androidx.multidex.MultiDex
import com.likandr.swapi.BuildConfig
import com.likandr.swapi.common.di.DaggerAppComponent
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class SwapiApp : DaggerApplication() {

    companion object {
        var instance: SwapiApp? = null
            private set
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(instance)) {
                return
            }
            LeakCanary.install(instance)
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}
