package com.likandr.swapi.common.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.Unbinder
import com.likandr.swapi.common.FragmentParams
import com.likandr.swapi.common.Layout
import com.likandr.swapi.common.util.ViewModelFactory
import dagger.android.support.DaggerFragment
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment() {

    private var unbinder: Unbinder? = null
    private var activity: AppCompatActivity? = null

    @set:Inject
    var viewModelFactory: ViewModelFactory? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val cls = javaClass
        if (!cls.isAnnotationPresent(Layout::class.java)) return null
        val annotation = cls.getAnnotation(Layout::class.java)
        val layout = annotation as Layout
        val view = inflater.inflate(layout.id, container, false)

        unbinder = ButterKnife.bind(this, view)

        this.getArgs(arguments)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.setUpToolbar()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as AppCompatActivity
    }

    override fun onDetach() {
        super.onDetach()
        activity = null
    }

    fun getBaseActivity(): AppCompatActivity {
        return activity!!
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (unbinder != null) {
            unbinder!!.unbind()
            unbinder = null
        }
    }

    //
    open fun getArgs(_bundle: Bundle?) {
        val fragmentName = this::class.qualifiedName
        println("fragment $fragmentName has raw arguments")
    }

    abstract fun setUpToolbar()

    override fun onStart() {
        super.onStart()
        beginWith()
    }

    abstract fun beginWith()

    public var changeFragmentSubject = PublishSubject.create<FragmentParams>()!!

    fun changeFragment(): Observable<FragmentParams> {
        return changeFragmentSubject
    }
}