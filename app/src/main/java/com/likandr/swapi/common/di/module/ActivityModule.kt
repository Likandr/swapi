package com.likandr.swapi.common.di.module

import com.likandr.swapi.ui.MainActivity
import com.likandr.swapi.ui.MainFragmentBindingModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    internal abstract fun bindMainActivity(): MainActivity
}