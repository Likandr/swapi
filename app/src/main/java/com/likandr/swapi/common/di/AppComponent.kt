package com.likandr.swapi.common.di

import android.app.Application
import com.likandr.swapi.common.SwapiApp
import com.likandr.swapi.common.di.module.ActivityModule
import com.likandr.swapi.common.di.module.ContextModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@SuppressWarnings
@Singleton
@Component(
    modules = [
        ContextModule::class,
        AppModule::class,
        AndroidSupportInjectionModule::class,
        ActivityModule::class
    ]
)
interface AppComponent : AndroidInjector<SwapiApp> {

    override fun inject(application: SwapiApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}
