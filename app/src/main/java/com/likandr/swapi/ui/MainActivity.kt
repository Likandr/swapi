package com.likandr.swapi.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.ListFragment
import com.likandr.swapi.R
import com.likandr.swapi.common.FragmentParams
import com.likandr.swapi.common.Layout
import com.likandr.swapi.common.base.BaseActivity
import com.likandr.swapi.common.base.BaseFragment
import com.likandr.swapi.data.People
import com.likandr.swapi.ui.detail.DetailFragment
import com.likandr.swapi.ui.main.SearchFragment

@Layout(id = R.layout.activity_main)
class MainActivity : BaseActivity() {
    override fun contentRes(): Int {
        return R.id.content_frame
    }

    override fun beginWith() {
        Log.d("MainActivity", "onSaveInstanceState")
//        initFragment(FragmentParams(SearchFragment.TAG, null, null))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MainActivity", "onCreate")
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction().add(contentRes(), SearchFragment.newInstance()).commit()
    }

    override fun getFragment(fragmentParams: FragmentParams): BaseFragment? {
        if (fragmentParams.fragmentTag.equals(DetailFragment.TAG)) {
            return DetailFragment.newInstance(
                fragmentParams.args as People,
                ""
            )
        }

        return SearchFragment.newInstance()
    }

    override fun onBackPressed() {
        if (currentFragmentTag == DetailFragment.TAG) {
            initFragment(FragmentParams(SearchFragment.TAG, null, null))
        } else {
            finish()
        }
    }
}
