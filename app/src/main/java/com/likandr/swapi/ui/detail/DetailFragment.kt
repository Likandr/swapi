package com.likandr.swapi.ui.detail

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProviders
import com.likandr.swapi.R
import com.likandr.swapi.common.Layout
import com.likandr.swapi.common.base.BaseFragment
import com.likandr.swapi.data.People
import com.likandr.swapi.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_detail.*

@Layout(id = R.layout.fragment_detail)
class DetailFragment : BaseFragment() {

    val TAG = DetailFragment::class.java.simpleName

    private var detailViewModel: DetailViewModel? = null

    companion object {
        val TAG = DetailFragment::class.qualifiedName!!

        private const val PEOPLE = "people"
        private const val EXTRA_TRANSITION_NAME = "transition_name"

        var mPeople: People? = null

        fun newInstance(people: People, transitionName: String) = DetailFragment().apply {
            arguments = bundleOf(
                PEOPLE to people,
                EXTRA_TRANSITION_NAME to transitionName
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detailViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(DetailViewModel::class.java)
        detailViewModel!!.restoreFromBundle(savedInstanceState)
    }

    override fun onSaveInstanceState(@NonNull outState: Bundle) {
        super.onSaveInstanceState(outState)
        detailViewModel!!.saveToBundle(outState)
    }

    override fun getArgs(_bundle: Bundle?) {
        mPeople = if (_bundle!!.containsKey(PEOPLE)) {
            _bundle.get(PEOPLE) as People?
        } else {
            null
        }
    }

    @Suppress("PLUGIN_WARNING")
    override fun setUpToolbar() {
        Log.d(DetailFragment.TAG, "setUpToolbar")
        setHasOptionsMenu(true)
        val activity = activity as MainActivity?

        if (activity != null) {
            val toolbar: Toolbar = app_bar!!
            activity.setSupportActionBar(toolbar)

            val actionBar = activity.supportActionBar
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                (activity as MainActivity).onBackPressed(); true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun beginWith() {

        tv_name.text = mPeople!!.name
        tv_birth_year.text = mPeople!!.birth_year
        tv_height.text = mPeople!!.height
        tv_mass.text = mPeople!!.mass
        tv_skin_color.text = mPeople!!.skin_color
        tv_hair_color.text = mPeople!!.hair_color
        tv_eye_color.text = mPeople!!.eye_color
        tv_gender.text = mPeople!!.gender
    }
}
