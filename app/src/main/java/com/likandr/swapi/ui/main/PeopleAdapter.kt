package com.likandr.swapi.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.likandr.swapi.R
import com.likandr.swapi.data.People
import kotlinx.android.synthetic.main.item_people.view.*

class PeopleAdapter(val context: FragmentActivity) : ListAdapter<People, PeopleViewHolder>(PeoplesDiff()) {

    private val clickedPeople: MutableLiveData<People> = MutableLiveData()

    private var inflater: LayoutInflater? = null

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleViewHolder {
        return PeopleViewHolder(inflater!!.inflate(R.layout.item_people, parent, false))
    }

    override fun onBindViewHolder(holder: PeopleViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            clickedPeople.value = getItem(position)
        }
    }

    fun getClickedPeople(): LiveData<People> {
        return clickedPeople
    }
}

//region ViewHolders
class PeopleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    init {
        ButterKnife.bind(itemView)
    }

    fun bind(people: People) {
        itemView.tv_people_name.text = people.name
        itemView.tv_hair_color.text = people.hair_color
        itemView.tv_birth_year.text = people.birth_year
    }
}
//endregion

class PeoplesDiff : DiffUtil.ItemCallback<People>() {
    override fun areItemsTheSame(oldItem: People, newItem: People): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: People, newItem: People): Boolean {
        return oldItem == newItem
    }
}
