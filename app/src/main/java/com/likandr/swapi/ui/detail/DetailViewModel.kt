package com.likandr.swapi.ui.detail

import android.os.Bundle
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class DetailViewModel @Inject constructor() : ViewModel() {
    fun saveToBundle(outState: Bundle) {
    }

    fun restoreFromBundle(savedInstanceState: Bundle?) {
    }
}
