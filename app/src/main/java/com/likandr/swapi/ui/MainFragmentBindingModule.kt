package com.likandr.swapi.ui

import com.likandr.swapi.ui.detail.DetailFragment
import com.likandr.swapi.ui.main.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    internal abstract fun provideSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    internal abstract fun provideDetailsFragment(): DetailFragment
}
