package com.likandr.swapi.ui.main

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.likandr.swapi.common.util.RxUtils
import com.likandr.swapi.data.People
import com.likandr.swapi.data.PeopleSearchResults
import com.likandr.swapi.data.api.SwapiApi
import com.likandr.swapi.data.db.PeopleDao
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val peopleApi: SwapiApi,
    private val peopleDao: PeopleDao
) : ViewModel() {
    private var compositeDisposable: CompositeDisposable? = null

    private val peoples: MutableLiveData<PeopleSearchResults> = MutableLiveData()
    private val peoplesDb: MutableLiveData<List<People>> = MutableLiveData()
    private val history: MutableLiveData<List<People>> = MutableLiveData()
    init {
        compositeDisposable = CompositeDisposable()
    }

    fun saveToBundle(outState: Bundle) {
    }

    fun restoreFromBundle(savedInstanceState: Bundle?) {
        loadHistory()
    }

    fun getPeoples(): LiveData<PeopleSearchResults> {
        return peoples
    }

    fun getPeoplesDb(): LiveData<List<People>> {
        return peoplesDb
    }

    fun getHistory(): LiveData<List<People>> {
        return history
    }

    //region History Search People
    fun loadHistory() {
        Log.d("ListViewModel", "loadHistory")
        val disposable =
            peopleDao.all
                .compose(RxUtils.getSchedulersForFlowable())
                .subscribe(
                    this::onHistorySuccess,
                    this::onHistoryError)

        compositeDisposable!!.add(disposable)
    }

    private fun onHistorySuccess(people : List<People>) {
        history.value = people
    }

    private fun onHistoryError(e : Throwable) {}
    //endregion History Search People

    //region Search People
    fun loadPeoples(queue: String) {
        Log.d("ListViewModel", "loadPeoples")

        val disposable =
            peopleApi.getPeoples(queue)
                .compose(RxUtils.getSchedulersForFlowable())
                .subscribe(
                    this::onSearchSuccess,
                    this::onSearchError)

        compositeDisposable!!.add(disposable)
    }

    private fun onSearchSuccess(result : PeopleSearchResults) {
        peoples.value = result
    }

    private fun onSearchError(e : Throwable) {}
    //endregion Search People

    override fun onCleared() {
        super.onCleared()
        if (compositeDisposable != null) {
            compositeDisposable!!.clear()
            compositeDisposable = null
        }
    }

    fun savePeople(people: People) {
        val disposable = Observable.just(peopleDao)
            .subscribeOn(Schedulers.io())
            .subscribe { it.insert(people) }

        compositeDisposable!!.add(disposable)
    }

    fun searchInDb(queue: String) {
        Log.d("ListViewModel", "searchInDb")

        val disposable =
            peopleDao.search("%$queue%")
                .compose(RxUtils.getSchedulersForFlowable())
                .subscribe(
                    this::onSearchDbSuccess,
                    this::onSearchError)

        compositeDisposable!!.add(disposable)
    }

    private fun onSearchDbSuccess(result: List<People>) {
        peoplesDb.value = result
    }
}
