package com.likandr.swapi.ui.main

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProviders
import com.likandr.swapi.R
import com.likandr.swapi.common.Layout
import com.likandr.swapi.common.base.BaseFragment
import com.likandr.swapi.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_search.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.likandr.swapi.common.FragmentParams
import com.likandr.swapi.common.util.Utils
import com.likandr.swapi.data.People
import com.likandr.swapi.data.PeopleSearchResults
import com.likandr.swapi.ui.detail.DetailFragment
import kotlinx.android.synthetic.main.content_history.*
import kotlinx.android.synthetic.main.content_search_result.*

@Layout(id = R.layout.fragment_search)
class SearchFragment : BaseFragment() {

    private var isAlreadyShowWarningNoNetwork = false

    private var searchViewModel: SearchViewModel? = null

    private var mSearchAdapter: PeopleAdapter? = null
    private var mHistoryAdapter: PeopleAdapter? = null

    companion object {
        val TAG = SearchFragment::class.qualifiedName!!

        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    override fun getArgs(_bundle: Bundle?) {}

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)

        onSetupMenu(menu)
    }

    var mSearchView: SearchView? = null
    private fun onSetupMenu(menu: Menu) {

        val searchItem = menu.findItem(R.id.action_search)
        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(menuItem: MenuItem): Boolean {
                showSearchResult(true)
                return true
            }

            override fun onMenuItemActionCollapse(menuItem: MenuItem): Boolean {
                showSearchResult(false)
                return true
            }
        })

        mSearchView = searchItem.actionView as SearchView
        mSearchView!!.maxWidth = Integer.MAX_VALUE
        mSearchView!!.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        mSearchView!!.setIconifiedByDefault(false)
        mSearchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                mSearchView!!.clearFocus()
                searchGO(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                searchGO(newText)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun searchGO(queue: String) {
        if (Utils.isNetworkAvailable(context!!)) {
            searchViewModel!!.loadPeoples(queue)
        } else {
            searchViewModel!!.searchInDb(queue)

            if (!isAlreadyShowWarningNoNetwork) {
                showNoticeSnack("Network is unavailable")
                isAlreadyShowWarningNoNetwork = !isAlreadyShowWarningNoNetwork
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(SearchViewModel::class.java)
        searchViewModel!!.restoreFromBundle(savedInstanceState)

        mSearchAdapter = PeopleAdapter(activity!!)
        rv_results.addItemDecoration(DividerItemDecoration(getBaseActivity(), DividerItemDecoration.VERTICAL))
        rv_results.adapter = mSearchAdapter
        rv_results.layoutManager = LinearLayoutManager(context)

        mHistoryAdapter = PeopleAdapter(activity!!)
        rv_history.addItemDecoration(DividerItemDecoration(getBaseActivity(), DividerItemDecoration.VERTICAL))
        rv_history.adapter = mHistoryAdapter
        rv_history.layoutManager = LinearLayoutManager(context)
    }

    override fun setUpToolbar() {
        Log.d(SearchFragment.TAG, "setUpToolbar")
        setHasOptionsMenu(true)
        val activity = activity as MainActivity?

        if (activity != null) {
            val toolbar: Toolbar = app_bar!!
            toolbar.inflateMenu(R.menu.menu_main)
            activity.setSupportActionBar(toolbar)

            val actionBar = activity.supportActionBar
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(false)
                actionBar.show()
            }
        }
    }

    override fun beginWith() {
        Log.d(TAG, "beginWith")

        //Подписываемся на апдейты результатов поиска
        searchViewModel!!.getPeoples().observe(this, Observer<PeopleSearchResults> { result ->
            mSearchAdapter!!.submitList(result.results)
            rv_results.visibility = View.VISIBLE
            tv_no_results.visibility = View.GONE
        })

        //Подписываемся на апдейты результатов поиска из базы(при отсутствии интернета)
        searchViewModel!!.getPeoplesDb().observe(this, Observer<List<People>> { result ->
            mSearchAdapter!!.submitList(result)
            rv_results.visibility = View.VISIBLE
            tv_no_results.visibility = View.GONE
        })

        //Подписываемся на апдейты истории просмотров
        searchViewModel!!.getHistory().observe(this, Observer<List<People>> { result ->
            mHistoryAdapter!!.submitList(result)
            if (result != null) rv_history.visibility = View.VISIBLE
            tv_no_history.visibility = View.GONE
        })

        mSearchAdapter!!.getClickedPeople().observe(this, Observer<People> {
            mSearchView!!.clearFocus()
            searchViewModel!!.savePeople(it)
            (activity as MainActivity).initFragment(FragmentParams(DetailFragment.TAG, it, null))
        })

        mHistoryAdapter!!.getClickedPeople().observe(this, Observer<People> {
            mSearchView!!.clearFocus()
            (activity as MainActivity).initFragment(FragmentParams(DetailFragment.TAG, it, null))
        })
    }

    fun showSearchResult(status: Boolean) {
        if (status) {
            results.visibility = View.VISIBLE
            history.visibility = View.GONE
        } else {
            results.visibility = View.GONE
            history.visibility = View.VISIBLE

            searchViewModel!!.loadHistory()
            isAlreadyShowWarningNoNetwork = !isAlreadyShowWarningNoNetwork
        }
    }

    private fun showNoticeSnack(text: String) {
        Snackbar.make(activity!!.findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT).show()
    }
}