package com.likandr.swapi.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.io.Serializable

@Entity
data class People(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var birth_year: String,
    var eye_color: String,
    @TypeConverters(StringConverter::class) var films: MutableList<String>,
    var gender: String,
    var hair_color: String,
    var height: String,
    var homeworld: String,
    var mass: String,
    var name: String,
    var skin_color: String,
    var created: String,
    var edited: String,
    @TypeConverters(StringConverter::class) var species: MutableList<String>,
    @TypeConverters(StringConverter::class)  var starships: MutableList<String>,
    var url: String,
    @TypeConverters(StringConverter::class) var vehicles: MutableList<String>
) : Serializable

data class PeopleSearchResults (
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<People>)
