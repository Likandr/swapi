package com.likandr.swapi.data.api

import com.likandr.swapi.data.PeopleSearchResults
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface SwapiApi {

    @GET("api/people/")
    fun getPeoples(@Query("search") search: String): Flowable<PeopleSearchResults>
}
