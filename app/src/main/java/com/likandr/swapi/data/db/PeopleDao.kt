package com.likandr.swapi.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.likandr.swapi.data.People
import io.reactivex.Flowable

@Dao
interface PeopleDao {
    @get:Query("SELECT * FROM people")
    val all: Flowable<List<People>>

    @Query("SELECT * FROM people WHERE name LIKE :name")
    fun search(name: String?): Flowable<List<People>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg people: People)

}