package com.likandr.swapi.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.likandr.swapi.data.People
import com.likandr.swapi.data.StringConverter

@Database(entities = [(People::class)], version = 1, exportSchema = false)
@TypeConverters(value = [
    StringConverter::class])
abstract class SwapiDatabase: RoomDatabase() {

    abstract fun getPeopleDao(): PeopleDao
}