package com.likandr.swapi.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StringConverter {
    private var gson = Gson()
    private var type = object : TypeToken<MutableList<String>>() {}.type

    @TypeConverter
    fun stringToList(json: String): MutableList<String> {
        return gson.fromJson<MutableList<String>>(json, type)
    }

    @TypeConverter
    fun listToString(budgetModel: MutableList<String>): String {
        return gson.toJson(budgetModel, type)
    }
}
